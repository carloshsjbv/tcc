//package br.com.carlos.projeto.conclusao.curso.repository;
//
//import br.com.carlos.projeto.conclusao.curso.model.ProfessorEntity;
//import org.springframework.data.repository.JpaRepository;
//import org.springframework.web.bind.annotation.CrossOrigin;
//
///**
// * Interface que implementa JpaRepository e é responsável pela comunicação com o
// * banco de dados, proporcionando assim, a criação de um CRUD de AdminEntity
// *
// * @author Carlos H
// */
//@CrossOrigin(origins = "http://localhost:4200")
//public interface ProfessorRepository extends JpaRepository<ProfessorEntity, Long> {
//
//}
